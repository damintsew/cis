package com.damintsev;

import com.damintsev.domain.Price;
import com.damintsev.service.MergePriceService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author adamintsev
 * @since 21.01.2017.
 */


public class MergePriceServiceTest {

    private final static SimpleDateFormat date = new SimpleDateFormat("dd.MM.yyyy");
    private final static SimpleDateFormat dateWithTime = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");

    private MergePriceService mergeService;

    @Before
    public void setup() {
        mergeService = new MergePriceService();
    }

    @Test
    public void emptyOldPricesTest() {
        List<Price> newPrices = getInitialNewPriceList(1, 1);

        List<Price> merged = mergeService.merge(newPrices, new ArrayList<Price>(0));

        Assert.assertArrayEquals("emptyOldPricesTest. Arrays don't equals", newPrices.toArray(), merged.toArray());
    }

    @Test
    public void emptyNewPricesTest() {
        List<Price> oldPrices = getInitialNewPriceList(1,1);

        List<Price> merged = mergeService.merge(new ArrayList<Price>(0), oldPrices);

        Assert.assertArrayEquals("emptyOldPricesTest. Arrays don't equals", oldPrices.toArray(), merged.toArray());
    }

    @Test
    public void nonIntersectionMergeTest() {
        List<Price> oldPrices = getInitialNewPriceList(1,1);
        List<Price> newPrices = getInitialNewPriceList(1,2);

        List<Price> merged = mergeService.merge(newPrices, oldPrices);

        Assert.assertEquals("Test merge without any intersection between prices. Arrays don't equals",
                oldPrices.size() + newPrices.size(), merged.size());
    }

    @Test
    public void nonIntersectionMergeWithMultiplePricesTest() {
        List<Price> oldPrices = getInitialNewPriceList(1,1);
        List<Price> newPrices = Arrays.asList(
                price(2,1, 1, "10.01.2017", "31.01.2017", "Kinder surp"),
                price(2,2, 2, "01.02.2017", "31.02.2017", "Snikers"),
                price(2,3, 3, "01.02.2017", "31.02.2017", "Mars")
        );

        List<Price> merged = mergeService.merge(newPrices, oldPrices);

        Assert.assertEquals("Test merge without any intersection between prices. Arrays don't equals",
                oldPrices.size() + newPrices.size(), merged.size());
    }


    @Test
    public void intersectionFromLeftTest() {

        String beginOld = "10.01.2017";
        String endOld = "31.01.2017";

        String beginNew = "01.01.2017";
        String endNew = "15.01.2017";

        String desiredBeginOld = "15.01.2017 00:00:01";
        String desiredEndOld = "31.01.2017 00:00:00";

        List<Price> oldPrices = Arrays.asList(
                price(2,1, 1, beginOld, endOld, "Kinder surp")
        );
        List<Price> newPrices = Arrays.asList(
                price(2, 1, 5, beginNew, endNew, "Kinder surp")
        );

        List<Price> merged = mergeService.merge(newPrices, oldPrices);

        Assert.assertEquals("Test merge intersection FROM LEFT prices. Arrays don't equals",
                oldPrices.size() + newPrices.size(), merged.size());

        //Doesn't changed. Same values as before
        Price addedPrice = find(merged, 5);
        Assert.assertEquals("For added Price 'Begin' time didn't matches", d("01.01.2017"), addedPrice.getBegin());
        Assert.assertEquals("For added Price 'End' time didn't matches", d("15.01.2017"), addedPrice.getEnd());

        //Changed left border from 10.01.2017 --> 15.01.2017 00:00:01
        Price modifiedPrice = find(merged, 1);
        Assert.assertEquals("For existing old Price 'Begin' time didn't matches", dt(desiredBeginOld), modifiedPrice.getBegin());
        Assert.assertEquals("For existing old 'End' time didn't matches", dt(desiredEndOld), modifiedPrice.getEnd());

    }

    @Test
    public void intersectionFromRightTest() {

        String beginOld = "10.01.2017";
        String endOld = "31.01.2017";

        String beginNew= "15.01.2017";
        String endNew = "30.02.2017";

        String desiredBeginOld = "10.01.2017 00:00:00";
        String desiredEndOld = "14.01.2017 23:59:59";

        List<Price> oldPrices = Arrays.asList(
                price(2,1, 1, beginOld, endOld, "Zamorojennaya riba")
        );
        List<Price> newPrices = Arrays.asList(
                price(2, 1, 2, beginNew, endNew, "Zamorojennaya riba")
        );

        List<Price> merged = mergeService.merge(newPrices, oldPrices);

        Assert.assertEquals("Test merge intersection FROM RIGHT between prices. Arrays don't equals",
                oldPrices.size() + newPrices.size(), merged.size());

        Price addedPrice = find(merged, 2);
        //Doesn't changed. Same values as before
        Assert.assertEquals("For added Price 'Begin' time didn't matches", d(beginNew), addedPrice.getBegin());
        Assert.assertEquals("For added Price 'End' time didn't matches", d(endNew), addedPrice.getEnd());

        Price modifiedPrice = find(merged, 1);
        //Changed RIGHT border from 31.01.2017 --> 15.01.2017
        Assert.assertEquals("For existing old Price 'Begin' time didn't matches", dt(desiredBeginOld), modifiedPrice.getBegin());
        Assert.assertEquals("For existing old 'End' time didn't matches", dt(desiredEndOld), modifiedPrice.getEnd());//todo refactor
    }

    @Test
    public void intersectionInMiddleTest() {

        String beginOld = "01.01.2017";
        String endOld = "31.01.2017";

        String beginNew= "10.01.2017";
        String endNew = "20.01.2017";

        String desiredOldFirstBegin = "01.01.2017 00:00:00";
        String desiredOldFirstEnd = "09.01.2017 23:59:59";

        String desiredOldSecondBegin = "20.01.2017 00:00:01";
        String desiredOldSecondEnd = "31.01.2017 00:00:00";

        List<Price> oldPrices = Arrays.asList(
                price(2,1, 1, beginOld, endOld, "Zamorojennaya riba")
        );
        List<Price> newPrices = Arrays.asList(
                price(2, 1, 2, beginNew, endNew, "Zamorojennaya riba")
        );

        List<Price> merged = mergeService.merge(newPrices, oldPrices);

        Assert.assertEquals("Test merge intersection in MIDDLE between prices. Arrays length increased by 1 due to we divided existing" +
                        "prices. Arrays don't equals",
                oldPrices.size() + newPrices.size() + 1, merged.size());

        Price addedPrice = find(merged, 2);
        Assert.assertEquals("For added Price 'Begin' time didn't matches", d(beginNew), addedPrice.getBegin());
        Assert.assertEquals("For added Price 'End' time didn't matches", d(endNew), addedPrice.getEnd());

        //Old price divided by two prices
        Price firstPartOfNewPrice = find(merged, 1);
        //Begin is the same. End changed from 31.01.2017 --> 09.01.2017 23:59:59
        Assert.assertEquals("For existing first part of old Price 'Begin' time didn't matches", dt(desiredOldFirstBegin), firstPartOfNewPrice.getBegin());
        Assert.assertEquals("For existing first part of old 'End' time didn't matches", dt(desiredOldFirstEnd), firstPartOfNewPrice.getEnd());

        Price secondPartOfNewPrice = find(merged, 0);
        //End is the same. Begin changed from 01.01.2017 --> 20.01.2017 00:00:01
        Assert.assertEquals("For existing second part of old Price 'Begin' time didn't matches", dt(desiredOldSecondBegin), secondPartOfNewPrice.getBegin());
        Assert.assertEquals("For existing second part of old 'End' time didn't matches", dt(desiredOldSecondEnd), secondPartOfNewPrice.getEnd());
    }

    @Test
    public void intersectionOverlayTest() {

        String beginOld = "02.01.2017";
        String endOld = "31.01.2017";

        String beginNew= "01.01.2017";
        String endNew = "05.02.2017";

        List<Price> oldPrices = Arrays.asList(
                price(2,1, 1, beginOld, endOld, "Pechenki")
        );
        List<Price> newPrices = Arrays.asList(
                price(2, 1, 2, beginNew, endNew, "Pechenki")
        );

        List<Price> merged = mergeService.merge(newPrices, oldPrices);

        //Due to new price overrides existing the result is the array of length
        Assert.assertEquals("Test merge intersection between prices. Arrays don't equals",
                newPrices.size(), merged.size());

        Price addedPrice = find(merged, 2);
        //checking only for new Price
        Assert.assertEquals("For added Price 'Begin' time didn't matches", d(beginNew), addedPrice.getBegin());
        Assert.assertEquals("For added Price 'End' time didn't matches", d(endNew), addedPrice.getEnd());
    }

    @Test
    public void intersectionOverlayWithExactDateMatchTest() {

        String beginOld = "01.01.2017";
        String endOld = "31.01.2017";

        String beginNew= "01.01.2017";
        String endNew = "05.02.2017";

        List<Price> oldPrices = Arrays.asList(
                price(2,1, 1, beginOld, endOld, "Pelmeni")
        );
        List<Price> newPrices = Arrays.asList(
                price(2, 1, 2, beginNew, endNew, "Pelmeni")
        );

        List<Price> merged = mergeService.merge(newPrices, oldPrices);

        //Due to new price overrides existing the result is the array of length
        Assert.assertEquals("Test merge intersection between prices. Arrays don't equals",
                newPrices.size(), merged.size());

        Price addedPrice = find(merged, 2);
        //checking only for new Price
        Assert.assertEquals("For added Price 'Begin' time didn't matches", d(beginNew), addedPrice.getBegin());
        Assert.assertEquals("For added Price 'End' time didn't matches", d(endNew), addedPrice.getEnd());
    }

    @Test
    public void intersectionRightWithLeft() {

        String firstBeginOld = "01.01.2017"; String desiredFirstBeginOld = "01.01.2017 00:00:00";
        String firstEndOld = "31.01.2017";   String desiredFirstEndOld = "14.01.2017 23:59:59";

        String secondBeginOld = "01.02.2017"; String desiredSecondBeginOld = "10.02.2017 00:00:01";
        String secondEndOld = "15.02.2017"; String desiredSecondEndOld = "15.02.2017 00:00:00";

        String beginNew = "15.01.2017";
        String endNew = "10.02.2017";

        List<Price> oldPrices = Arrays.asList(
                price(2,1, 1, firstBeginOld, firstEndOld, "Makaroshki"),
                price(2,1, 2, secondBeginOld, secondEndOld, "Makaroshki")
        );
        List<Price> newPrices = Arrays.asList(
                price(2, 1, 3, beginNew, endNew, "Makaroshki")
        );

        List<Price> merged = mergeService.merge(newPrices, oldPrices);

        //Due to new price overrides existing the result is the array of length
        Assert.assertEquals("Test merge intersection between prices. Arrays don't equals",
                newPrices.size() + oldPrices.size(), merged.size());

        Price addedPrice = find(merged, 3);
        //checking only for new Price
        Assert.assertEquals("For added Price 'Begin' time didn't matches", d(beginNew), addedPrice.getBegin());
        Assert.assertEquals("For added Price 'End' time didn't matches", d(endNew), addedPrice.getEnd());

        Price oldFirstPrise = find(merged, 1);
        //checking only for new Price
        Assert.assertEquals("For added Price 'Begin' time didn't matches", dt(desiredFirstBeginOld), oldFirstPrise.getBegin());
        Assert.assertEquals("For added Price 'End' time didn't matches", dt(desiredFirstEndOld), oldFirstPrise.getEnd());

        Price oldSecondPrise = find(merged, 2);
        //checking only for new Price
        Assert.assertEquals("For added Price 'Begin' time didn't matches", dt(desiredSecondBeginOld), oldSecondPrise.getBegin());
        Assert.assertEquals("For added Price 'End' time didn't matches", dt(desiredSecondEndOld), oldSecondPrise.getEnd());
    }

    //for test purposes this is fine implementation
    protected Price find(List<Price> list, long key) {
        for(Price p : list) {
            if (p.getId() == key) {
                return p;
            }
        }
        return null;
    }

    private List<Price> getInitialNewPriceList(int depart, int priceNumber) {

        List<Price> prices = new ArrayList<>();

        Price p = new Price();
        p.setDepart(depart);
        p.setNumber(priceNumber);
        p.setId(1);
        p.setProduct_code("Mayones");
        p.setBegin(d("01.01.2017"));
        p.setEnd(d("01.10.2017"));

        prices.add(p);

        return prices;
    }

    private Price price(int depart, int priceNumber, long id, String begin, String end, String pcode) {
        Price p = new Price();
        p.setDepart(depart);
        p.setNumber(priceNumber);
        p.setId(id);
        p.setProduct_code(pcode);
        p.setBegin(d(begin));
        p.setEnd(d(end));

        return p;
    }

    private Date d(String stringDate) {

        try {
            return date.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    private Date dt(String stringDate) {

        try {
            return dateWithTime.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
