package com.damintsev.service;

import com.damintsev.domain.Price;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.damintsev.service.IntersectionType.*;

/**
 * @author adamintsev
 * @since 21.01.2017.
 */
public class MergePriceService {


    public List<Price> merge(List<Price> newPrices, List<Price> oldPrices) {

        //todo add validation ?

        if (newPrices.size() == 0) {
            return oldPrices;
        }

        if (oldPrices.size() == 0) {
            return newPrices;
        }

        newPrices = new ArrayList<>(newPrices);
        oldPrices = new ArrayList<>(oldPrices);

        final List<Price> mergeList = new ArrayList<>(oldPrices.size() + newPrices.size());
        final List<IntersectionResult> intersectionResults = new ArrayList<>();

        for(Price newP : newPrices) {
            List<Price> oldPricesWithSameDepartment = oldPrices.stream()
                    .filter(priceNumberAndDepartmentFilter(newP.getNumber(), newP.getDepart()))
                    .collect(Collectors.toList());

            List<IntersectionResult> intersections = findIntersections(newP, oldPricesWithSameDepartment);
            intersectionResults.addAll(intersections);
        }

        oldPrices.sort((o1, o2) -> (int)(o1.getId() - o2.getId()));
        for(IntersectionResult intersection : intersectionResults) {
            intersection.handle(oldPrices);
        }

        mergeList.addAll(oldPrices);
        mergeList.addAll(newPrices);

        return mergeList;
    }

    private List<IntersectionResult> findIntersections(Price newP, List<Price> oldPrices) {

        final List<IntersectionResult> intersections = new ArrayList<>();

        for(Price old : oldPrices) {

            IntersectionType result = calculateIntersection(newP, old);

            switch (result) {
                case LEFT: {
                    IntersectionLeftResult res = new IntersectionLeftResult();
                    res.setPriceId(old.getId());
                    res.newLeftBorderTime(newP.getEnd());

                    intersections.add(res);
                    break;
                }
                case MIDDLE: {
                    IntersectionMiddleResult res = new IntersectionMiddleResult();
                    res.setPriceId(old.getId());
                    res.setNewLeftBorder(newP.getBegin());
                    res.setNewRightBorder(newP.getEnd());

                    intersections.add(res);
                    break;
                }

                case RIGHT: {
                    IntersectionRightResult res = new IntersectionRightResult();
                    res.setPriceId(old.getId());
                    res.newRightBorderTime(newP.getBegin());

                    intersections.add(res);
                    break;
                }
                case OVERLAY: {
                    IntersectionOverlap res = new IntersectionOverlap();
                    res.setPriceId(old.getId());

                    intersections.add(res);
                }
            }
        }

        return intersections;
    }

    private IntersectionType calculateIntersection(Price newP, Price oldP) {
        Date newLeft = newP.getBegin();
        Date newRight = newP.getEnd();

        //Detect if there are any intersection
        if (newLeft.after(oldP.getEnd()) || newRight.before(oldP.getBegin())) {
            return NONE;
        }

        //Found intersections. Detect what type of intersctions
        boolean isStartNewBeforeStartOld = newLeft.getTime() <= oldP.getBegin().getTime();
        boolean isEndNewBeforeEndOld = newRight.getTime() <= oldP.getEnd().getTime();

        if (isStartNewBeforeStartOld && isEndNewBeforeEndOld) {
            //Intersection from LEFT. new: 01.01 - 20.01; old 10.01 - 30.01
            return  LEFT;
        } else if (!isStartNewBeforeStartOld && isEndNewBeforeEndOld) {
            //Intersection in MIDDLE. new: 10.01 - 20.01; old 01.01 - 30.01
            return MIDDLE;
        } else if (!isStartNewBeforeStartOld && !isEndNewBeforeEndOld) {
            //Intersection from RIGHT. new: 20.01 - 30.01; old 01.01 - 15.01
            return RIGHT;
        } else if (isStartNewBeforeStartOld && !isEndNewBeforeEndOld) {
            //Intersection OVERLAY. new: 01.01 - 20.01; old 10.01 - 15.01
            return OVERLAY;
        }

        throw new RuntimeException(String.format("Can't calculate intersection for prices " +
                "new: {start: %s, end: %s}, old: {start: %s, end: %s}", newLeft, newRight, oldP.getBegin(), oldP.getEnd()));
    }

    private Predicate<Price> priceNumberAndDepartmentFilter(int number, int depart) {
        return price -> price.getNumber() == number
                && price.getDepart() == depart;
    }
}
