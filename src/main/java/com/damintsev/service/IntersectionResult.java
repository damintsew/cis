package com.damintsev.service;

import com.damintsev.domain.Price;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author adamintsev
 * @since 22.01.2017.
 */
public abstract class IntersectionResult {

    protected long priceId;
    protected final static short ONE_SECOND = 1000;

    public void setPriceId(long priceId) {
        this.priceId = priceId;
    }

    public long getPriceId() {
        return priceId;
    }

    public void handle(List<Price> pricesToModify) {
        Price priceToModify = find(pricesToModify, priceId);

        handle(priceToModify);
    }

    protected void handle(Price priceToModify) {

    }

    //todo replace this to binary search implementation
    protected Price find(List<Price> list, long key) {
        for(Price p : list) {
            if (p.getId() == key) {
                return p;
            }
        }
        return null;
    }
}

/**
 * Move endTime (right border) for oldPrice
 */
class IntersectionRightResult extends IntersectionResult {

    private Date newTime;

    public void newRightBorderTime(Date newTime) {
        this.newTime = newTime;
    }

    @Override
    public void handle(Price priceToModify) {
        priceToModify.setEnd(new Date(newTime.getTime() - ONE_SECOND));
    }
}

/**
 * Move beginTime (left border) for oldPrice
 */
class IntersectionLeftResult extends IntersectionResult {

    private Date newTime;

    public void newLeftBorderTime(Date newTime) {
        this.newTime = newTime;
    }

    @Override
    public void handle(Price priceToModify) {
        priceToModify.setBegin(new Date(newTime.getTime() + ONE_SECOND));
    }
}

/**
 * Intersection in the middle. We should separate price into two.
 */
class IntersectionMiddleResult extends IntersectionResult {

    private Date newLeftBorder;
    private Date newRightBorder;

    public void setNewLeftBorder(Date newLeftBorder) {
        this.newLeftBorder = newLeftBorder;
    }

    public void setNewRightBorder(Date newRightBorder) {
        this.newRightBorder = newRightBorder;
    }

    @Override
    public void handle(List<Price> pricesToModify) {
        Price priceToModify = find(pricesToModify, getPriceId());
        Price newPrice = copy(priceToModify);
        newPrice.setId(0);//todo this is the new price. DB should tell us new Id

        //setting left border
        priceToModify.setEnd(new Date(newLeftBorder.getTime() - ONE_SECOND));
        //setting right border
        newPrice.setBegin(new Date(newRightBorder.getTime() + ONE_SECOND));

        pricesToModify.add(newPrice);
    }

    private Price copy(Price price) {
        Price newPrice = new Price();

        newPrice.setId(price.getId());
        newPrice.setNumber(price.getNumber());
        newPrice.setDepart(price.getDepart());
        newPrice.setProduct_code(price.getProduct_code());
        newPrice.setBegin(price.getBegin());
        newPrice.setEnd(price.getEnd());
        newPrice.setValue(price.getValue());

        return newPrice;
    }
}

/**
 * Remove oldPrice due to new completely replaced
 */
class IntersectionOverlap extends IntersectionResult {

    @Override
    public void handle(List<Price> pricesToModify) {
        pricesToModify.removeIf(price -> price.getId() == getPriceId());
    }
}

enum IntersectionType {
    LEFT, RIGHT, MIDDLE, OVERLAY, NONE
}