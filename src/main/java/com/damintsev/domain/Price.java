package com.damintsev.domain;

import java.util.Date;

/**
 * @author adamintsev
 * @since 21.01.2017.
 */
public class Price {

    private long id; // ������������� � ��
    private String product_code; // ��� ������
    private int number; // ����� ����
    private int depart; // ����� ������
    private Date begin; // ������ ��������
    private Date end; // ����� ��������
    private  long value; // �������� ���� � ��������

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getDepart() {
        return depart;
    }

    public void setDepart(int depart) {
        this.depart = depart;
    }

    public Date getBegin() {
        return begin;
    }

    public void setBegin(Date begin) {
        this.begin = begin;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }
}


